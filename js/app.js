//instanciamos un objeto calculadora v0.1
let calculator = new Calculator();
//Creamos nuestro div principal
calculator.createMainDiv();
//Creamos nuestro input donde recibirá numeros y el operador
calculator.createInput();
//creamos los botones que contendrán numeros
calculator.createAllNumberBtn();
//Creamos los botones que contendrán operadores
calculator.createDivOperators();
//calculator.hideDivOperators();//esconderá los botones
//calculator.showDivOperators();//mostrará los botones
//Creamos los botones que contendrán las flechas izquierda y derecha
calculator.createArrows();
calculator.hideDivArrows();//escondera los botones
//calculator.showDivArrows();//mostrará los botones
//creamos el div de los mensajes
calculator.createDivMsgs();