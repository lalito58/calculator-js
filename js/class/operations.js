//clase que contendrá las operaciones básicas: suma, resta, multiplicación, división
class Operations {
    
    constructor(){
        this.a = '';
        this.b = '';
        this.operator = 0;
    }

    get add(){
        return parseInt(this.a, 0) + parseInt(this.b, 0);
    }

    get substract(){
        return parseInt(this.a, 0) - parseInt(this.b, 0);
    }

    get divide(){
        if((this.b > 0)){
            return parseInt(this.a, 0) / parseInt(this.b, 0);
        }
        return;
    }

    get multiply(){
        return parseInt(this.a, 0) * parseInt(this.b, 0);
    }

    //metodos getter and setter
    get getValueA(){
        return this.a;
    }

    get getValueB(){
        return this.b;
    }

    get getOperator(){
        return this.operator;
    }

    set setValueA(value){
        this.a = value;
    }

    set setValueB(value){
        this.b = value;
    }

    set setOperator(value){
        this.operator = value;
    }
}