//clase que crea la interfaz de la calculadora: caja de texto, botones con
class Calculator extends Operations{

    constructor(){
        super();
        console.log("Clase de Calculator");
        this.idMainDiv = "mainDiv";
        this.idInput = "mainInput";
        this.msg = "msg";
    }

    createMainDiv(){
        let textHeader = document.createElement('h2');
        textHeader.textContent = "Calculadora v0.1";
        document.body.appendChild(textHeader);
        let div = document.createElement('div');
        div.id = this.idMainDiv;
        document.body.appendChild(div)
    }
    
    createInput(){
        let input = document.createElement('textarea');
        input.id = this.idInput;
        input.disabled = true;
        document.querySelector('#'+this.idMainDiv).appendChild(input);
        document.querySelector('#'+this.idInput).focus();
    }
    
    createAllNumberBtn(){
        const TOTAL_BTN = 10;
        const SEPARATOR = 3;
        
        let divBtn = document.createElement('div');
        divBtn.id = 'divBtn';
        divBtn.className = 'btnClass';
        document.querySelector('#'+this.idMainDiv).appendChild(divBtn);

        //let tmpCounter = 0;
        for ( let i = (TOTAL_BTN - 1) ; i >= 0 ; i-- ) {
            let btn = document.createElement('button');
            btn.id = 'btn-'+i;
            btn.className = 'btns';
            btn.textContent = i;
            
            btn.addEventListener('click', (function(e){
                document.querySelector('#'+this.idInput).append(e.target.textContent);
                
                if(this.getOperator === 0){
                    let a = this.getValueA;
                    a += e.target.textContent;
                    this.setValueA = a;
                }else{
                    let b = this.getValueB;
                    b += e.target.textContent;
                    this.setValueB = b;
                }
            }).bind(this));
            document.querySelector('#divBtn').appendChild(btn);
        }
    }
    createDivOperators(){
        let divOperators = document.createElement('div');
        divOperators.className = 'btnClass';
        divOperators.id = 'divOperators';
        document.querySelector('#'+this.idMainDiv).appendChild(divOperators);
        const OPERATORS = [
            { id: 'btnDelete', textBtn: 'C' },
            { id: 'btnMultiply', textBtn: '/' },
            { id: 'btnAdd', textBtn: '+' },
            { id: 'btnSubstract', textBtn: '-' },
            { id: 'btnDivide', textBtn: '*' },
            { id: 'btnEqual', textBtn: '=' }
        ];

        OPERATORS.forEach(function(operator){
            let btnOperator = document.createElement('button');
            btnOperator.id = operator.id;
            btnOperator.className = 'btns'
            btnOperator.textContent = operator.textBtn;

            btnOperator.addEventListener('click', (function(e){
                if(this.getValueA !== ''){
                    if(e.target.textContent !== '=' && e.target.textContent !== 'C'){
                        if(this.getOperator === 0){
                            document.querySelector('#'+this.idInput).append(e.target.textContent);
                            if(e.target.textContent === '+'){
                                this.setOperator = 1;
                            }else if(e.target.textContent === '-'){
                                this.setOperator = 2;
                            }else if(e.target.textContent === '*'){
                                this.setOperator = 3;
                            }else if(e.target.textContent === '/'){
                                this.setOperator = 4;
                            }else{
                                console.log(":(");
                            }
                        }
                    }else{
                        if(e.target.textContent === '='){
                            document.querySelector('#'+this.msg).innerHTML = '';
                            let result = 0;
                            switch (this.getOperator) {
                                case 1:
                                    result = this.add;
                                    this.setValueA = result;
                                    this.setValueB = '';
                                    this.setOperator = 0;
                                    break;
                                case 2:
                                    result = this.substract;
                                    this.setValueA = result;
                                    this.setValueB = '';
                                    this.setOperator = 0;
                                    break;
                                case 3:
                                    result = this.multiply;
                                    this.setValueA = result;
                                    this.setValueB = '';
                                    this.setOperator = 0;
                                    break;
                                case 4:
                                    result = this.divide;
                                    if(result !== undefined){
                                        this.setValueA = result;    
                                    }else{
                                        result = '';
                                        this.setValueA = '';
                                        document.querySelector('#'+this.msg).innerHTML = '<hr><b>No existe división entre 0!</b>';
                                    }
                                    this.setValueB = '';
                                    this.setOperator = 0;
                                    break;
                                default:
                                    console.log("Clean");
                                    break;
                            }
                            document.querySelector('#'+this.idInput).innerHTML = result;
                        }else if(e.target.textContent === 'C'){
                            //Clean input
                            document.querySelector('#'+this.idInput).innerHTML = "";
                            this.setValueA = '';
                            this.setValueB = '';
                            this.setOperator = 0;
                        }
                    }
                }
            }).bind(this));
            document.querySelector('#divOperators').appendChild(btnOperator);
        }.bind(this));

    }

    hideDivOperators(){
        document.querySelector('#divOperators').style.display = 'none';
    }

    showDivOperators(){
        document.querySelector('#divOperators').style.display = 'initial';
    }

    createArrows(){
        let divArrows = document.createElement('div');
        divArrows.id = 'divArrows';
        divArrows.className = 'arrowsClass';
        document.querySelector('#'+this.idMainDiv).appendChild(divArrows);
        const ARROWS = [
            { id: 'btnRight', textArrow: '=>' },
            { id: 'btnLeft', textArrow: '<=' } 
        ];

        ARROWS.forEach(function(arrow){
            let btnArrow = document.createElement('button');
            btnArrow.id = arrow.id;
            btnArrow.addEventListener('click', function(e){
                console.log(e.target.textContent);
            });
            btnArrow.textContent = arrow.textArrow;
            document.querySelector('#divArrows').appendChild(btnArrow);
        });
    }

    hideDivArrows(){
        document.getElementById('divArrows').style.display = 'none';
    }

    showDivArrows(){
        document.getElementById('divArrows').style.display = 'initial';
    }

    createDivMsgs(){
        let divArrows = document.createElement('div');
        divArrows.id = this.msg;
        divArrows.className = 'msgClass';
        document.querySelector('#'+this.idMainDiv).appendChild(divArrows);
    }
}